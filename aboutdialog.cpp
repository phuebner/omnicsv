#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include "version.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    QString versionString;
    versionString.append(QString::number(VER_MAJOR));
    versionString.append(".");
    versionString.append(QString::number(VER_MINOR));
    versionString.append(".");
    versionString.append(QString::number(VER_BUGFIX));
    ui->versionLabel->setText(versionString);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

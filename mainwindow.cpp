#include<QtWidgets>

#include "mainwindow.h"
#include "csvparser.h"
#include "preferencedialog.h"
#include "aboutdialog.h"

MainWindow::MainWindow(QMainWindow *parent) : QMainWindow(parent)
{
    csvData = NULL;

    pref = new PreferenceDialog();
    readSettings();

    browseButton = createButton(tr("&Browse..."), SLOT(browse()));
    directoryComboBox = createComboBox(QDir::currentPath());
    directoryLineEdit = new QLineEdit();
    directoryLineEdit->setPlaceholderText(tr("Click Browse... to select .DAT directory"));
    directoryLineEdit->setReadOnly(true);


    directoryLabel = new QLabel(tr("Directory:"));

    createFilesTable();
    createActions();
    createMenus();
    createToolBars();

//    QGridLayout *mainLayout = new QGridLayout;
//    mainLayout->setSizeConstraint(QLayout::SetNoConstraint);
//    mainLayout->addWidget(directoryLabel,0,0);
//    //mainLayout->addWidget(directoryComboBox,0,0);
//    mainLayout->addWidget(directoryLineEdit,0,1);
//    mainLayout->addWidget(browseButton,0,2);
//    mainLayout->addWidget(filesTable, 1,0,1,3);
//    setLayout(mainLayout);
    setCentralWidget(treeView);
    setWindowTitle(tr("CVS - Generator"));

    statusBar()->showMessage(tr("Ready"));
    loadOmniaxDatabase();
    resize(750, 500);

    connect(this->treeView,SIGNAL(pressed(QModelIndex)), this, SLOT(updateSelectIndicator()));
}

MainWindow::~MainWindow()
{

}
void MainWindow::createActions()
{
    openAct = new QAction(QIcon(":/icons/open2.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setToolTip(tr("Import .DAT files from folder"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(browse()));

    prefAct = new QAction(QIcon(":/icons/preferences2.png"), tr("&Preferences"), this);
    prefAct->setShortcut(QKeySequence::Preferences);
    connect(prefAct, SIGNAL(triggered()), this, SLOT(showPreferences()));

    aboutAct = new QAction(QIcon(":/icons/about.png"), tr("&About"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(showAboutBox()));

    exportSingleCSV = new QAction(QIcon(":/icons/csv_grey.png"), tr("&Export CSV"), this);
    connect(exportSingleCSV, SIGNAL(triggered()), this, SLOT(exportSelectedItems()));

    exportGroup = new QAction(QIcon(":/icons/group.png"), tr("&Export All"), this);
    connect(exportGroup, SIGNAL(triggered()), this, SLOT(exportAllItems()));

    showOmniaxDatabaseAct = new QAction(QIcon(":/icons/database2.png"), tr("&Database"), this);
    showOmniaxDatabaseAct->setToolTip(tr("Show the Omniax database"));
    connect(showOmniaxDatabaseAct, SIGNAL(triggered()), this, SLOT(showOmniaxDatabase()));

    deselectAllAct = new QAction(tr("&Deselect All"), this);
    deselectAllAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_D));
    connect(deselectAllAct, SIGNAL(triggered()), this->treeView, SLOT(clearSelection()));
    connect(deselectAllAct, SIGNAL(triggered()), this, SLOT(updateSelectIndicator()));

    expandSelectedAct = new QAction(tr("&Expand Selected"), this);
    expandSelectedAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Down));
    connect(expandSelectedAct, SIGNAL(triggered()), this, SLOT(expandSelected()));

    expandAllAct = new QAction(tr("&Expand All"), this);
    expandAllAct->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_Down));
    connect(expandAllAct, SIGNAL(triggered()), this->treeView, SLOT(expandAll()));

    collapseAllAct = new QAction(tr("&Collapse All"), this);
    collapseAllAct->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_Up));
    connect(collapseAllAct, SIGNAL(triggered()), this->treeView, SLOT(collapseAll()));

    collapseSelectedAct = new QAction(tr("&Collapse Selected"), this);
    collapseSelectedAct->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Up));
    connect(collapseSelectedAct, SIGNAL(triggered()), this, SLOT(collapseSelected()));

}
void MainWindow::createMenus()
{
    menuBar = new QMenuBar(0);

    preferenceMenu = new QMenu(tr("&Preferences"));
    preferenceMenu->addAction(prefAct);

    aboutMenu = new QMenu(tr("&About"));
    aboutMenu->addAction(aboutAct);

    editMenu = new QMenu(tr("&Edit"));
    editMenu->addAction(deselectAllAct);
    editMenu->addAction(expandSelectedAct);
    editMenu->addAction(expandAllAct);
    editMenu->addAction(collapseSelectedAct);
    editMenu->addAction(collapseAllAct);

    menuBar->addMenu(preferenceMenu);
    menuBar->addMenu(aboutMenu);
    menuBar->addMenu(editMenu);

}

void MainWindow::createToolBars()
{
    this->setUnifiedTitleAndToolBarOnMac(true);
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(openAct);
    fileToolBar->addAction(showOmniaxDatabaseAct);
    fileToolBar->addAction(exportSingleCSV);
    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    // toolBar is a pointer to an existing toolbar
    fileToolBar->addWidget(spacer);
    fileToolBar->addAction(prefAct);
    fileToolBar->addAction(aboutAct);
    fileToolBar->setMovable(false);
    fileToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
}

QPushButton *MainWindow::createButton(const QString &text, const char *member)
{
    QPushButton *button = new QPushButton(text);
    connect(button, SIGNAL(clicked()), this, member);
    return button;
}

QComboBox *MainWindow::createComboBox(const QString &text)
{
    QComboBox *comboBox = new QComboBox;
    comboBox->setEditable(true);
    comboBox->addItem(text);
    comboBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_MAEMO_5) || defined(Q_WS_SIMULATOR)
    comboBox->setMinimumContentsLength(3);
#endif
    return comboBox;
}

void MainWindow::createFilesTable()
{
//    filesTable = new QTableWidget(0,2);
//    filesTable->setSelectionBehavior(QAbstractItemView::SelectRows);

//    QStringList labels;
//    labels << tr("Filename") << tr("Omniax ID");
//    filesTable->setHorizontalHeaderLabels(labels);
//    filesTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
//    filesTable->verticalHeader()->hide();
//    filesTable->setShowGrid(false);

      treeView = new QTreeView();
      omniaxItemModel = new OmniaxItemModel();
      treeView->setModel(omniaxItemModel);
      //treeView->header()->setStretchLastSection(false);
      treeView->setColumnWidth(0,270);
      treeView->setColumnWidth(1,190);
      treeView->setColumnWidth(2,50);
      treeView->setColumnWidth(3,50);
      treeView->setColumnWidth(4,60);
      treeView->setColumnWidth(5,60);
      treeView->setColumnWidth(6,1);
      treeView->setColumnWidth(7,50);
      treeView->setColumnWidth(8,1);
      //treeView->header()->setResizeMode(0,QHeaderView::ResizeToContents);
      treeView->setAttribute(Qt::WA_MacShowFocusRect, false);
      treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
      treeView->setSelectionMode(QAbstractItemView::MultiSelection);
}

void MainWindow::updateFilesTable()
{
    QStringList files;
    //omniaxItemModel->clear();
    omniaxItemModel->removeRows(0, omniaxItemModel->rowCount());
    files = currentDirectory.entryList(QStringList("*"), QDir::Files | QDir::NoSymLinks);

    for(int index = 0; index < files.size(); ++index) {
        QStandardItem *fileNameItem = new QStandardItem(files[index]);
        omniaxItemModel->appendDatFile(fileNameItem);
    }
    updateOmniaxItemModel();
}

void MainWindow::browse()
{
    QString directory = QFileDialog::getExistingDirectory(this,
                                     tr("Select folder containing .DAT files ..."),
                                     QDir::currentPath());

    if(!directory.isEmpty()) {
        currentDirectory = QDir(directory);
        updateFilesTable();
    }
}

void MainWindow::loadOmniaxDatabase()
{
    if(!omniaxCvsFilePath.isEmpty()) {
        csv = new CsvParser();
        csvData = new QStandardItemModel;
        csvData = csv->parseFromFile(omniaxCvsFilePath);
        statusBar()->showMessage(tr("Ready  -  Omniax database loaded."));
    } else {
        statusBar()->showMessage(tr("Omniax database file not found!"));
    }
}

void MainWindow::showOmniaxDatabase()
{
    csvInspector = new CsvInspector();
    csvInspector->setModel(csvData);
    csvInspector->show();
    csvInspector->setFocus();
}

void MainWindow::readSettings()
{
    omniaxCvsFilePath = pref->getOmniaxDatabasePath();
}

void MainWindow::showPreferences()
{
    pref->exec();
    omniaxCvsFilePath = pref->getOmniaxDatabasePath();
    loadOmniaxDatabase();
    updateOmniaxItemModel();
}

void MainWindow::showAboutBox()
{
    AboutDialog about;
    about.setWindowTitle(tr("About"));
    about.exec();
    //QMessageBox::about(this,tr("About"), tr("Developed by Patrick Huebner"));
}

void MainWindow::updateOmniaxItemModel()
{
    if (omniaxItemModel) {
        omniaxItemModel->setOmniaxDatabaseModel(csvData);
        omniaxItemModel->setOrientationModel(pref->getOrientationModel());
        //treeView->update();
        treeView->setModel(omniaxItemModel);
    }
}

void MainWindow::exportSelectedItems()
{
    QModelIndexList indexes = treeView->selectionModel()->selectedRows();
    QString directory = QFileDialog::getExistingDirectory(this,
                                     tr("Specify Save Directory ..."),
                                     QDir::currentPath());
    omniaxItemModel->exportSelectedItems(indexes, QDir(directory));
}

void MainWindow::exportAllItems()
{

}

void MainWindow::expandSelected()
{
    QModelIndexList indexes = treeView->selectionModel()->selectedRows();

    while (!indexes.isEmpty())
    {
        treeView->expand(indexes.last());
        indexes.removeLast();
    }
}

void MainWindow::collapseSelected()
{
    QModelIndexList indexes = treeView->selectionModel()->selectedRows();

    while (!indexes.isEmpty())
    {
        QModelIndex parentIndex = indexes.last();
        if(parentIndex.parent().isValid())
            parentIndex = parentIndex.parent();

        treeView->collapse(parentIndex);
        //treeView->selectionModel()->select(parentIndex);
        //treeView->selectionModel()->select(parentIndex, QItemSelectionModel::Select|QItemSelectionModel::Rows);
        indexes.removeLast();
    }
}

void MainWindow::updateSelectIndicator()
{
    //if(index.parent().isValid()) {
        QItemSelectionModel *selModel = treeView->selectionModel();
        QModelIndexList selected = selModel->selectedIndexes();

        omniaxItemModel->clearHighligh();
        while (!selected.isEmpty()) {
            if(selected.last().parent().isValid()) {
                omniaxItemModel->setHighligh(selected.last().parent());
            }
            selected.removeLast();
        }


}

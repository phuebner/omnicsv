#include "csvinspector.h"
#include <QHBoxLayout>
#include <QDebug>

CsvInspector::CsvInspector(QWidget *parent) :
    QWidget(parent)
{
    p_model = new QStandardItemModel();
    tableView = new QTableView();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(tableView);
    setLayout(layout);
    resize(640,500);
}

void CsvInspector::setModel(QStandardItemModel *model)
{
    if(model != NULL) {
        p_model = model;
        tableView->setModel(p_model);
    }
}

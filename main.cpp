#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("NeuRA");
    a.setApplicationName("OmniCVS");
    MainWindow w;
    w.show();

    return a.exec();
}

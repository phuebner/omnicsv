#include "csvparser.h"
#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include <QFileDialog>
#include <QDebug>

CsvParser::CsvParser(QObject *parent) :
    QObject(parent)
{
}

CsvParser::CsvParser(const QString &filename)
{
    parseFromFile(filename);
}

QStandardItemModel *CsvParser::parseFromFile(const QString &filename, const QString &codec)
{
    QString string;
    parseHeader = true;
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly)) {
        model = new QStandardItemModel();
        while(!file.atEnd()) {
            QTextStream in(&file);
            in.setCodec(QTextCodec::codecForName(codec.toLatin1()));
            string = in.readAll();
            parse(initString(string));
        }
        file.close();
    }
    return model;
}

void CsvParser::parse(const QString &string)
{
    enum State {Normal, Quote} state = Normal;
    QString columnItem;
    standardItemList.clear();

    for (int i = 0; i < string.size(); i++) {

        QChar current = string.at(i);

        // Normal
        if (state == Normal) {
            // newline
            if (current == '\n') {
                    // Add column item to row
                    QStandardItem *item = new QStandardItem(columnItem);
                    standardItemList.append(item);
                    columnItem.clear();
                if (!parseHeader) {
                    // Finish row and start next
                    model->appendRow(standardItemList);
                    standardItemList.clear();
                } else {
                    for (int i = 0; i < standardItemList.size(); ++i) {
                        model->setHorizontalHeaderItem(i,standardItemList.at(i));
                    }
                    standardItemList.clear();
                    parseHeader = false;
                }
            }
            // comma
            else if (current == ',') {
                // Add column item to row
                QStandardItem *item = new QStandardItem(columnItem);
                standardItemList.append(item);
                columnItem.clear();
            }
            // double quote
            else if (current == '"') {
                state = Quote;
            }
            // character
            else {
                columnItem += current;
            }
        }
        // Quote
        else if (state == Quote) {
            // double quote
            if (current == '"') {
                int index = (i+1 < string.size()) ? i+1 : string.size();
                QChar next = string.at(index);
                if (next == '"') {
                    columnItem += '"';
                    i++;
                } else {
                    state = Normal;
                }
            }
            // other
            else {
                columnItem += current;
            }
        }
    }
}

QString CsvParser::initString(const QString &string)
{
    QString result = string;
    result.replace("\r\n", "\n"); // Windows line ending to linux line ending
    result.replace("\r", "\n"); // Old mac line ending to linux line ending
    int resultSize = result.size();
    if (result.at(resultSize-1) != '\n') {
        result += '\n';
    }
    return result;
}

void CsvParser::writeCSV(const QStandardItemModel *model, QString filename_in)
{
    QString filename;
    if (filename_in.isEmpty())
        filename = QFileDialog::getSaveFileName(0, "Save CSV file", "filename.csv", "CSV files (*.csv);;Zip files (*.zip, *.7z)", 0, 0); // getting the filename (full path)
    else
        filename = filename_in;

    QFile data(filename);
    if(data.open(QFile::WriteOnly |QFile::Truncate)) {
        QTextStream output(&data);
        qDebug() << model->columnCount() << " - " << model->rowCount();
        /* Write Header */
        for (int headerIndex = 0 ; headerIndex < model->columnCount() ; ++headerIndex) {
            output << model->horizontalHeaderItem(headerIndex)->text();
            if ( headerIndex != (model->columnCount())-1 )
                output << ",";
            else
                output << "\n";
        }

        /* Write Body */
        for (int rowIndex = 0 ; rowIndex < model->rowCount() ; ++rowIndex) {
            for (int columnIndex = 0 ; columnIndex < model->columnCount() ; ++columnIndex) {
                output << model->item(rowIndex,columnIndex)->text();
                if ( columnIndex != (model->columnCount())-1 )
                    output << ",";
                else
                    output << "\n";
            }
        }
        data.close();
    }
}

#ifndef CSVMANAGER_H
#define CSVMANAGER_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QString>
#include <QStandardItemModel>

class CSVManager : public QObject
{
    Q_OBJECT
public:
    explicit CSVManager(QObject *parent = 0);

signals:

public slots:
    QList<QStringList> csv;
    QStandardItemModel *model;
    QList<QStandardItem*> standardItemList;
};

#endif // CSVMANAGER_H

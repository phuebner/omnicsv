#ifndef OMNIAXITEMMODEL_H
#define OMNIAXITEMMODEL_H

#include <QStandardItemModel>
#include <QVariant>
#include <QDir>


class OmniaxItemModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit OmniaxItemModel(QObject *parent = 0);

    void appendDatFile(QStandardItem *datFile);
    void setOmniaxDatabaseModel(QStandardItemModel *omniaxDatabaseModel);
    void setOrientationModel(QStandardItemModel *model_in);
    void identifyOrientations();
    void clear();

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    void exportSelectedItems(QModelIndexList indexes, QDir directory);
signals:

public slots:
    void setHighligh(const QModelIndex &index);
    void clearHighligh();

private:
    void createHeader();
    QString *extractVisitID(QString fileName);
    QString findOrientation(int alpha, int beta);

    QList<QStandardItem*> standardItemList;
    QStandardItem *nameHeaderLabel;
    QStandardItem *moveHeaderLabel;
    QStandardItemModel *orientationModel;

    bool checked;
};

#endif // OMNIAXITEMMODEL_H

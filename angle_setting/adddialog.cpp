#include "adddialog.h"
#include "ui_adddialog.h"

AddDialog::AddDialog(QString identifier, int alpha, int alphaRange, int beta, int betaRange, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
    connect(ui->addButton, SIGNAL(clicked()), this, SLOT(accept()));
    connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(reject()));

    ui->positionNameEdit->setText(identifier);
    ui->alphaSpinBox->setValue(alpha);
    ui->alphaRangeSpinBox->setValue(alphaRange);
    ui->betaSpinBox->setValue(beta);
    ui->betaRangeSpinBox->setValue(betaRange);
}

AddDialog::~AddDialog()
{
    delete ui;
}

QList<QStandardItem *> *AddDialog::getPosition()
{
    QList<QStandardItem *> *list = new QList<QStandardItem *>;
    QStandardItem *positionName = new QStandardItem(ui->positionNameEdit->text());
    list->append(positionName);
    QStandardItem *alpha = new QStandardItem(ui->alphaSpinBox->text());
    list->append(alpha);
    QStandardItem *alphaRange = new QStandardItem(ui->alphaRangeSpinBox->text());
    list->append(alphaRange);
    QStandardItem *beta = new QStandardItem(ui->betaSpinBox->text());
    list->append(beta);
    QStandardItem *betaRange = new QStandardItem(ui->betaRangeSpinBox->text());
    list->append(betaRange);

    return list;
}

QString AddDialog::getIdentifier() { return ui->positionNameEdit->text(); }
int AddDialog::getAlpha() { return ui->alphaSpinBox->value(); }
int AddDialog::getAlphaRange() { return ui->alphaRangeSpinBox->value(); }
int AddDialog::getBeta() { return ui->betaSpinBox->value(); }
int AddDialog::getBetaRange() { return ui->betaRangeSpinBox->value(); }

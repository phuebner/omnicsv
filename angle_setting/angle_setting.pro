#-------------------------------------------------
#
# Project created by QtCreator 2014-06-17T22:20:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = angle_setting
TEMPLATE = app


SOURCES += main.cpp\
        anglecontrol.cpp \
    adddialog.cpp

HEADERS  += anglecontrol.h \
    adddialog.h

FORMS    += \
    adddialog.ui

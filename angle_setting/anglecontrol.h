#ifndef ANGLECONTROL_H
#define ANGLECONTROL_H

#include <QWidget>
#include <QStandardItemModel>
#include <QTableView>
#include <QPushButton>


class AngleControl : public QWidget
{
    Q_OBJECT

public:
    explicit AngleControl(QWidget *parent = 0);
    ~AngleControl();

    QStandardItemModel *getModel();
    void setModel(QStandardItemModel *model_in);

private slots:
    void addPosition();
    void editPosition();
    void deletePosition();
    void saveOrientations();
    void loadOrientations();

private:
    void createHeader();

    QStandardItemModel *model;
    QTableView *tableView;
    QPushButton *addButton;
    QPushButton *editButton;
    QPushButton *deleteButton;

    QPushButton *saveButton;
    QPushButton *loadButton;
};

#endif // ANGLECONTROL_H

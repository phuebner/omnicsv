#include "anglecontrol.h"
#include "adddialog.h"

#include <QStandardItemModel>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>

#include "../csvparser.h"

AngleControl::AngleControl(QWidget *parent) :
    QWidget(parent)
{
    model = new QStandardItemModel();

    tableView = new QTableView();
    tableView->setModel(model);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->setAttribute(Qt::WA_MacShowFocusRect, false);
    createHeader();
    addButton = new QPushButton(tr("Add"));
    connect(addButton, SIGNAL(clicked()), this, SLOT(addPosition()));

    editButton = new QPushButton(tr("Edit"));
    connect(editButton, SIGNAL(clicked()), this, SLOT(editPosition()));

    deleteButton = new QPushButton(tr("Delete"));
    connect(deleteButton, SIGNAL(clicked()), this, SLOT(deletePosition()));

    saveButton = new QPushButton(tr("Save"));
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveOrientations()));

    loadButton = new QPushButton(tr("Load"));
    connect(loadButton, SIGNAL(clicked()), this, SLOT(loadOrientations()));

    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(tableView);
    QVBoxLayout *buttonLayout = new QVBoxLayout();
    buttonLayout->addWidget(addButton);
    buttonLayout->addWidget(editButton);
    buttonLayout->addWidget(deleteButton);
    buttonLayout->addSpacing(20);
    buttonLayout->addWidget(saveButton);
    buttonLayout->addWidget(loadButton);
    buttonLayout->addStretch();
    layout->addLayout(buttonLayout);
    layout->setMargin(0);

    setLayout(layout);
}

AngleControl::~AngleControl()
{
}

QStandardItemModel *AngleControl::getModel()
{
    return model;
}

void AngleControl::saveOrientations()
{
    CsvParser parser;
    parser.writeCSV(model);
}

void AngleControl::loadOrientations()
{
    QString filename = QFileDialog::getOpenFileName (this, "Open Orientation file",
                                                     QDir::currentPath(), "CSV (*.csv)");
    if (!filename.isEmpty()) {
        CsvParser parser;
        model = parser.parseFromFile(filename);
        tableView->setModel(model);
    }
}


void AngleControl::setModel(QStandardItemModel *model_in)
{
    model = model_in;
    createHeader();
    tableView->setModel(model);
}

void AngleControl::addPosition()
{
    AddDialog *addDialog = new AddDialog();
    if(addDialog->exec()) {
        model->appendRow(*addDialog->getPosition());
    }
}

void AngleControl::editPosition()
{
    QModelIndexList indexes = tableView->selectionModel()->selectedRows();
    if (indexes.size() > 1)
        QMessageBox::warning(this, tr("Edit not possible"), tr("You can only edit one position at a time"));
    else if (indexes.size() < 1)
        QMessageBox::warning(this, tr("Edit not possible"), tr("You need to select an item"));
    else {
        int selectedRow = indexes.last().row();
        QModelIndex identifierIndex = tableView->model()->index(selectedRow, 0, QModelIndex());
        QVariant identifier = tableView->model()->data(identifierIndex, Qt::DisplayRole);

        QModelIndex alphaIndex = tableView->model()->index(selectedRow, 1, QModelIndex());
        QVariant alpha = tableView->model()->data(alphaIndex, Qt::DisplayRole);

        QModelIndex alphaRangeIndex = tableView->model()->index(selectedRow, 2, QModelIndex());
        QVariant alphaRange = tableView->model()->data(alphaRangeIndex, Qt::DisplayRole);

        QModelIndex betaIndex = tableView->model()->index(selectedRow, 3, QModelIndex());
        QVariant beta = tableView->model()->data(betaIndex, Qt::DisplayRole);

        QModelIndex betaRangeIndex = tableView->model()->index(selectedRow, 4, QModelIndex());
        QVariant betaRange = tableView->model()->data(betaRangeIndex, Qt::DisplayRole);

        qDebug() << selectedRow << " - " <<identifier.toString() << " - " << alpha.toInt();

        AddDialog *addDialog = new AddDialog(identifier.toString(),
                                             alpha.toInt(),
                                             alphaRange.toInt(),
                                             beta.toInt(),
                                             betaRange.toInt());
        addDialog->setWindowTitle(tr("Edit Orientation"));
        if (addDialog->exec()) {
            tableView->model()->setData(identifierIndex, addDialog->getIdentifier(), Qt::EditRole);
            tableView->model()->setData(alphaIndex, addDialog->getAlpha(), Qt::EditRole);
            tableView->model()->setData(alphaRangeIndex, addDialog->getAlphaRange(), Qt::EditRole);
            tableView->model()->setData(betaIndex, addDialog->getBeta(), Qt::EditRole);
            tableView->model()->setData(betaRangeIndex, addDialog->getBetaRange(), Qt::EditRole);
        }
    }
}

void AngleControl::deletePosition()
{
   QModelIndexList indexes = tableView->selectionModel()->selectedRows();
   while (!indexes.isEmpty())
   {
       model->removeRows(indexes.last().row(), 1);
       indexes.removeLast();
   }

}

void AngleControl::createHeader()
{
    tableView->setColumnWidth(0,210);
    tableView->setColumnWidth(1,65);
    tableView->setColumnWidth(2,65);
    tableView->setColumnWidth(3,65);
    tableView->setColumnWidth(4,65);
 //   tableView->setColumnWidth(5,1);


    model->setHorizontalHeaderItem(0, new QStandardItem("Name"));
    model->setHorizontalHeaderItem(1, new QStandardItem("Alpha"));
    model->setHorizontalHeaderItem(2, new QStandardItem("A-Range"));
    model->setHorizontalHeaderItem(3, new QStandardItem("Beta"));
    model->setHorizontalHeaderItem(4, new QStandardItem("B-Range"));
//model->setHorizontalHeaderItem(5, new QStandardItem(""));
}

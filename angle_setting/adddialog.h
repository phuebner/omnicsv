#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include <QStandardItem>
#include <QList>

namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT
public:
    AddDialog(QString identifier = "", int alpha = 0, int alphaRange = 10, int beta = 0, int betaRange = 10, QWidget *parent = 0);
    ~AddDialog();

    QList<QStandardItem * > *getPosition();

    QString getIdentifier();
    int getAlpha();
    int getAlphaRange();
    int getBeta();
    int getBetaRange();

signals:

public slots:

private:
    Ui::AddDialog *ui;
};

#endif // ADDDIALOG_H

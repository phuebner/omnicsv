#include "preferencedialog.h"
#include <QGridLayout>
#include <QDir>
#include <QFileDialog>

PreferenceDialog::PreferenceDialog(QWidget *parent) :
    QDialog(parent)
{
    QLabel *header = new QLabel(tr("Preferences"));
    header->setFont(QFont("Arial", 25, QFont::Bold));

    omniaxDirLabel = new QLabel(tr("Omniax Database:"));
    omniaxDirBrowseButton = new QPushButton(tr("Browse ..."));
    connect(omniaxDirBrowseButton, SIGNAL(clicked()), this, SLOT(browse()));
    omniaxDirLineEdit = new QLineEdit(omniaxDatabasePath);
    closeButton = new QPushButton(tr("Close"));
    connect(closeButton, SIGNAL(clicked()), this, SLOT(closeDialog()));
    angleControl = new AngleControl();
    QLabel *angelControlLabel = new QLabel(tr("Orientations:"));

    QGridLayout *gLayout = new QGridLayout;
    gLayout->addWidget(omniaxDirLabel, 0, 0);
    gLayout->addWidget(omniaxDirLineEdit, 1, 0);
    gLayout->addWidget(omniaxDirBrowseButton, 1,1);
    gLayout->addWidget(angelControlLabel, 2, 0);
    gLayout->addWidget(angleControl, 3, 0, 1,2);

    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->addStretch(1);
    buttonsLayout->addWidget(closeButton);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(header);
    mainLayout->addSpacing(10);
    mainLayout->addLayout(gLayout);
    mainLayout->addSpacing(12);
    mainLayout->addLayout(buttonsLayout);
    setLayout(mainLayout);

    setWindowTitle(tr("Preferences"));

    settings = new QSettings("NeuRA", "OmniCVS");
    readSettings();

    resize(640, 500);
}

void PreferenceDialog::readSettings()
{
    omniaxDatabasePath = settings->value("omniaxCvsFilePath", "").toString();
    omniaxDirLineEdit->setText(omniaxDatabasePath);

    QStandardItemModel *model = new QStandardItemModel();
    settings->beginGroup("Omniax Orientations");
        int size = settings->beginReadArray("Orientations");
        for(int i=0;i<size;i++){
            settings->setArrayIndex(i);
            QStandardItem *name = new QStandardItem(settings->value("name").toString());
            QStandardItem *alpha = new QStandardItem(settings->value("alpha").toString());
            QStandardItem *alphaRange = new QStandardItem(settings->value("alpha_range").toString());
            QStandardItem *beta = new QStandardItem(settings->value("beta").toString());
            QStandardItem *betaRange = new QStandardItem(settings->value("beta_range").toString());

            QList<QStandardItem *> list;
            list << name << alpha << alphaRange << beta << betaRange;
            model->appendRow(list);
        }
        settings->endArray();
    settings->endGroup();
    angleControl->setModel(model);
}

void PreferenceDialog::writeSettings()
{
    QStandardItemModel *model = angleControl->getModel();

    settings->setValue("omniaxCvsFilePath", omniaxDatabasePath);
    settings->beginGroup("Omniax Orientations");
        settings->beginWriteArray("Orientations");
            for (int i = 0 ; i < model->rowCount() ; ++i) {
                settings->setArrayIndex(i);
                settings->setValue("name", model->item(i,0)->text());
                settings->setValue("alpha", model->item(i,1)->text());
                settings->setValue("alpha_range", model->item(i,2)->text());
                settings->setValue("beta", model->item(i,3)->text());
                settings->setValue("beta_range", model->item(i,4)->text());
            }
        settings->endArray();
    settings->endGroup();
}

QString PreferenceDialog::getOmniaxDatabasePath()
{
    return omniaxDatabasePath;
}

QStandardItemModel *PreferenceDialog::getOrientationModel()
{
    return angleControl->getModel();
}

void PreferenceDialog::closeDialog()
{
    writeSettings();
    close();
}

void PreferenceDialog::browse()
{
    omniaxDatabasePath = QFileDialog::getOpenFileName (this, "Open CSV file",
                                                       QDir::currentPath(), "CSV (*.csv)");
    omniaxDirLineEdit->setText(omniaxDatabasePath);
}

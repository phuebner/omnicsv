#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeView>
#include <QTableWidgetItem>
#include <QComboBox>
#include <QPushButton>
#include <QDir>
#include <QLabel>
#include <QTextStream>
#include <QStandardItemModel>
#include <QAction>
#include <QMenuBar>
#include "csvparser.h"
#include "csvinspector.h"
#include "preferencedialog.h"
#include "omniaxitemmodel.h"

QT_BEGIN_NAMESPACE
class QComboBox;
class QPushButton;
class QTableWidget;
class QDir;

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QMainWindow *parent = 0);
    ~MainWindow();

private slots:
    void browse();
    void showPreferences();
    void showAboutBox();
    void showOmniaxDatabase();
    void exportSelectedItems();
    void exportAllItems();
    void expandSelected();
    void collapseSelected();

    void updateSelectIndicator();

private:
    void createActions();
    void createMenus();
    void createToolBars();

    void updateOmniaxItemModel();

    QAction *openAct;
    QAction *prefAct;
    QAction *aboutAct;
    QAction *exportSingleCSV;
    QAction *exportGroup;
    QAction *showOmniaxDatabaseAct;

    QAction *deselectAllAct;
    QAction *expandSelectedAct;
    QAction *expandAllAct;
    QAction *collapseSelectedAct;
    QAction *collapseAllAct;

    QPushButton *createButton(const QString &text, const char *member);
    QComboBox *createComboBox(const QString &text = QString());
    void createFilesTable();
    void updateFilesTable();
    QString*extractOmniaxID(QString fileName);
    void loadOmniaxDatabase();
    void readSettings();

    QMenuBar *menuBar;
    QMenu *preferenceMenu;
    QMenu *aboutMenu;
    QMenu *fileMenu;
    QMenu *editMenu;

    QToolBar *fileToolBar;
    QComboBox *directoryComboBox;
    QLineEdit *directoryLineEdit;
    QPushButton *browseButton;
    QTreeView *treeView;

    QLabel *directoryLabel;

    QDir currentDirectory;
    CsvParser *csv;
    QStandardItemModel *csvData;
    CsvInspector *csvInspector;

    QString omniaxCvsFilePath;
    PreferenceDialog *pref;

    OmniaxItemModel *omniaxItemModel;
};

#endif // MAINWINDOW_H

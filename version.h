#ifndef VERSION_H
#define VERSION_H

#define VER_MAJOR                   0
#define VER_MINOR                   1
#define VER_BUGFIX                  0

#endif // VERSION_H

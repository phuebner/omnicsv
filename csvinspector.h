#ifndef CSVINSPECTOR_H
#define CSVINSPECTOR_H

#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>

class CsvInspector : public QWidget
{
    Q_OBJECT
public:
    explicit CsvInspector(QWidget *parent = 0);

    void setModel(QStandardItemModel *model);
signals:

public slots:

private:
    QTableView *tableView;
    QStandardItemModel *p_model;
};

#endif // CSVINSPECTOR_H

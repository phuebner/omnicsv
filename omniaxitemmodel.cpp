#include "omniaxitemmodel.h"
#include <QDebug>
#include "csvparser.h"
#include <QDir>
#include <QFile>

OmniaxItemModel::OmniaxItemModel(QObject *parent) :
    QStandardItemModel(parent)
{
    createHeader();
    orientationModel=NULL;
}

void OmniaxItemModel::setOmniaxDatabaseModel(QStandardItemModel *omniaxDatabaseModel)
{
    if(omniaxDatabaseModel != NULL) {
        for (int i = 0 ; i < rowCount() ; ++i) {
            QModelIndex datIndex = index(i,0);
            if (hasChildren(datIndex)) {
                removeRows(0, rowCount(datIndex), datIndex);
            }

            //visitID should be in column 3 of Model
            QString visitID = item(i,7)->text();
            if ( !visitID.isEmpty() ) {
                QList<QStandardItem* > databaseRows = omniaxDatabaseModel->findItems(visitID, Qt::MatchExactly, 1);
                for (int rowIndex = 0 ; rowIndex < databaseRows.size() ; ++rowIndex) {
                    QModelIndex databaseIndex = omniaxDatabaseModel->indexFromItem(databaseRows.at(rowIndex));

                    QStandardItem *description = new QStandardItem("");
                    //description->setCheckable(true);
                    QStandardItem *moveName = new QStandardItem(omniaxDatabaseModel->item(databaseIndex.row(), 3)->text());
                    int alpha = omniaxDatabaseModel->item(databaseIndex.row(), 4)->text().toInt();
                    int beta = omniaxDatabaseModel->item(databaseIndex.row(), 5)->text().toInt();

                    if (alpha > 180)
                        alpha = alpha - 360;
                    else if (alpha < -180)
                        alpha = alpha + 360;

                    if (beta > 180)
                        beta = beta - 360;
                    else if (beta < -180)
                        beta = beta + 360;

                    QStandardItem *alphaItem = new QStandardItem(QString::number(alpha));
                    QStandardItem *betaItem = new QStandardItem(QString::number(beta));
                    QStandardItem *startFrame = new QStandardItem(omniaxDatabaseModel->item(databaseIndex.row(), 8)->text());
                    QStandardItem *stopFrame = new QStandardItem(omniaxDatabaseModel->item(databaseIndex.row(), 9)->text());
                    int start = startFrame->text().toInt();
                    int stop = stopFrame->text().toInt();
                    int duration = stop-start;
                    QStandardItem *durationItem = new QStandardItem(QString::number(duration));

                    QList<QStandardItem* > itemList;
                    itemList.append(description);
                    itemList.append(moveName);
                    itemList.append(alphaItem);
                    itemList.append(betaItem);
                    itemList.append(startFrame);
                    itemList.append(durationItem);

                    item(i)->appendRow(itemList);
                }
                identifyOrientations();
            }
        }
    }
}
void OmniaxItemModel::identifyOrientations()
{
    for (int i = 0 ; i < rowCount() ; ++i) {
        QModelIndex visitID = index(i, 0);

        if (visitID.isValid()) {
            for (int j = 0 ; j < rowCount(visitID) ; ++j) {
                QModelIndex alphaIndex = visitID.child(j,2);
                int alpha = data(alphaIndex).toInt();

                QModelIndex betaIndex = visitID.child(j,3);
                int beta = data(betaIndex).toInt();

                QString orientationName = findOrientation(alpha, beta);

                QModelIndex descriptionIndex = visitID.child(j,0);
                setData(descriptionIndex, orientationName);
            }
        }
    }
}

void OmniaxItemModel::appendDatFile(QStandardItem *datFile)
{
    standardItemList.append(datFile);

    // Leave two columns empty for Move Data
    for (int i = 0 ; i < 6 ; ++i) {
        QStandardItem *emptyItem = new QStandardItem(" ");
        standardItemList.append(emptyItem);
    }

    // Extract and append Visit ID
    QString *visitID = extractVisitID(datFile->text());
    QStandardItem *visitIDItem = new QStandardItem("");
    visitIDItem->setTextAlignment(Qt::AlignCenter);
    if(visitID != NULL) {
        visitIDItem->setText(*visitID);
        visitIDItem->setBackground(QBrush(QColor(144,255,170)));
    }
    standardItemList.append(visitIDItem);

    // Add another empty column for stretch
    QStandardItem *emptyItem = new QStandardItem("");
    standardItemList.append(emptyItem);

    // Now append everything to OmniaxItemModel
    appendRow(standardItemList);
    standardItemList.clear();
}

QString *OmniaxItemModel::extractVisitID(QString fileName)
{
    QString *visitID = NULL;

    if(!fileName.isEmpty()) {
        QRegExp rx("_([0-9]{1,5})");
        if(rx.indexIn(fileName) != -1)
            visitID = new QString(rx.cap(1));
    }
    return visitID;
}

void OmniaxItemModel::createHeader()
{
    nameHeaderLabel = new QStandardItem(tr("Name"));
    moveHeaderLabel = new QStandardItem(tr("Move"));

    QStandardItem *startHeaderLabel = new QStandardItem(tr("Start"));
    QStandardItem *durationHeaderLabel = new QStandardItem(tr("Duration"));
    QStandardItem *visitIDHeaderLabel = new QStandardItem(tr("Visit ID"));
    setHorizontalHeaderItem(0,nameHeaderLabel);
    setHorizontalHeaderItem(1,new QStandardItem(tr("Move Name")));
    setHorizontalHeaderItem(2,new QStandardItem(tr("Alpha")));
    setHorizontalHeaderItem(3,new QStandardItem(tr("Beta")));
    setHorizontalHeaderItem(4,startHeaderLabel);
    setHorizontalHeaderItem(5,durationHeaderLabel);
    setHorizontalHeaderItem(6,new QStandardItem(""));
    setHorizontalHeaderItem(7,visitIDHeaderLabel);
    setHorizontalHeaderItem(8, new QStandardItem(""));
}

void OmniaxItemModel::clear()
{
    createHeader();
}

void OmniaxItemModel::setOrientationModel(QStandardItemModel *model_in)
{
    orientationModel = model_in;
    identifyOrientations();
}

void OmniaxItemModel::exportSelectedItems(QModelIndexList indexes, QDir directory)
{
    QString lastParent = "";
    QString currentParent = "";
    QStandardItemModel *exportModel = NULL;

    while (!indexes.isEmpty())
    {
        QModelIndex parentI = indexes.last().parent();
        QModelIndex nameIndex;
        int selectedRow = indexes.last().row();
        if (!parentI.isValid())
            parentI = QModelIndex();

        nameIndex = index(selectedRow, 0, parentI);
        //nameIndex = index(selectedRow, 0);
        qDebug() << data(nameIndex).toString();
        if(hasChildren(nameIndex)) {
            /* The selected item is a Dat file -- Export all moves with name if no move is selected */
        } else if(nameIndex.parent().isValid()) {
            /* Item is a move */
            QModelIndex parentIndex = nameIndex.parent();
            qDebug() << parentIndex.isValid() << " - " << parentIndex.row() << " - " << parentIndex.column();
            currentParent = data(parentIndex).toString();
            if(currentParent == lastParent) {
                /* Item belongs to same DAT file and should be grouped together */
                QList<QStandardItem *> itemList;
                QModelIndex manoeuvreIndex = index(selectedRow, 0, parentI);
                QString manoeuvreName = data(manoeuvreIndex).toString();
                if (!manoeuvreName.isEmpty()) {
                    itemList.append(new QStandardItem(manoeuvreName)); /* Manoeuvre */
                    QModelIndex startIndex = index(selectedRow, 4, parentI);
                    itemList.append(new QStandardItem(data(startIndex).toString())); /* Start */
                    QModelIndex lengthIndex = index(selectedRow, 5, parentI);
                    itemList.append(new QStandardItem(data(lengthIndex).toString())); /* Length */
                    exportModel->appendRow(itemList);
                }
            } else {
                /* Belongs to new DAT file */

                if (exportModel) {
                    /* Export Model has content -> save to csv */

                    QString filename = lastParent.replace(lastParent.length()-3, 3, "csv");
                    QString filepath = directory.filePath(filename);
                    CsvParser csv;
                    csv.writeCSV(exportModel, filepath);
                }

                delete exportModel; // Save some memory
                exportModel = new QStandardItemModel();
                /* Generate export model header */
                exportModel->setHorizontalHeaderItem(0,new QStandardItem("Manoeuvre"));
                exportModel->setHorizontalHeaderItem(1,new QStandardItem("Start"));
                exportModel->setHorizontalHeaderItem(2,new QStandardItem("Length"));

                QList<QStandardItem *> itemList;
                QModelIndex manoeuvreIndex = index(selectedRow, 0, parentI);
                QString manoeuvreName = data(manoeuvreIndex).toString();
                if (!manoeuvreName.isEmpty()) {
                    itemList.append(new QStandardItem(manoeuvreName)); /* Manoeuvre */
                    QModelIndex startIndex = index(selectedRow, 4, parentI);
                    itemList.append(new QStandardItem(data(startIndex).toString())); /* Start */
                    QModelIndex lengthIndex = index(selectedRow, 5, parentI);
                    itemList.append(new QStandardItem(data(lengthIndex).toString())); /* Length */
                    exportModel->appendRow(itemList);
                }
            }
            lastParent = currentParent;
        }
        indexes.removeLast();
    }
    if (exportModel) {
        /* Export Model has content -> save to csv */
        QString filename = lastParent.replace(lastParent.length()-3, 3, "csv");
        QString filepath = directory.filePath(filename);
        CsvParser csv;
        csv.writeCSV(exportModel, filepath);
    }

    delete exportModel; // Save some memory
}

QString OmniaxItemModel::findOrientation(int alpha, int beta)
{
    QString name = "";

    if(orientationModel) {
        for (int i = 0 ; i < orientationModel->rowCount() ; ++i) {
            int m_alpha = orientationModel->item(i,1)->text().toInt();
            int m_alphaRange = orientationModel->item(i,2)->text().toInt();
            int m_beta = orientationModel->item(i,3)->text().toInt();
            int m_betaRange = orientationModel->item(i,4)->text().toInt();

            int alpha_low = m_alpha-m_alphaRange;
            int alpha_high = m_alpha+m_alphaRange;
            int beta_low = m_beta-m_betaRange;
            int beta_high = m_beta+m_betaRange;

            if ( alpha > alpha_low && alpha < alpha_high && beta > beta_low && beta < beta_high) {
                name = orientationModel->item(i,0)->text();
                break;
            }
        }
    }
    return name;
}

QVariant OmniaxItemModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();

//        if (role == Qt::BackgroundRole) {
//            if (0 == index.row() % 2)
//               return QColor(226, 237, 253);
//            else
           //return QColor(Qt::white);
//
    //QStandardItem *item = static_cast<QStandardItem*>(index.internalPointer());

    //if ( role == Qt::CheckStateRole && index.column() == 0 )
    //    return static_cast< int >( item->isChecked() ? Qt::Checked : Qt::Unchecked );

    if (role == Qt::DecorationRole) {
        int col = index.column();
        if (col == 0 && !index.parent().isValid()) {
            return QIcon(":/icons/person.png");
        }
        if (col == 0 && index.parent().isValid()) {
            //return QIcon(":/icons/rotate.png");
        }
    }

    //if (role != Qt::DisplayRole)
    //    return QVariant();

    return QStandardItemModel::data(index, role);
}

Qt::ItemFlags OmniaxItemModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

    if ( index.column() == 0 )
        flags |= Qt::ItemIsUserCheckable;

    return flags;
}

void OmniaxItemModel::setHighligh(const QModelIndex &index)
{
    for (int i = 0 ; i < (columnCount() - 2) ; ++i ) {
        item(index.row(), i)->setBackground(QBrush(QColor(223,223,255)));
    }
}

void OmniaxItemModel::clearHighligh()
{
    for (int i = 0 ; i < rowCount() ; ++i) {
        for (int j = 0 ; j < (columnCount() - 2) ; ++j ) {
            item(i, j)->setBackground(QBrush(Qt::white));
        }
    }
}

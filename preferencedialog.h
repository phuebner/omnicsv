#ifndef PREFERENCEDIALOG_H
#define PREFERENCEDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QSettings>

#include "angle_setting/anglecontrol.h"

class PreferenceDialog : public QDialog
{
    Q_OBJECT
public:
    PreferenceDialog(QWidget *parent = 0);

    QString getOmniaxDatabasePath();
    QStandardItemModel *getOrientationModel();

signals:

public slots:

private slots:
    void closeDialog();
    void browse();

private:
    void readSettings();
    void writeSettings();

    QString omniaxDatabasePath;

    QLabel *omniaxDirLabel;
    QPushButton *omniaxDirBrowseButton;
    QLineEdit *omniaxDirLineEdit;

    QPushButton *closeButton;
    QSettings *settings;

    AngleControl *angleControl;
};

#endif // PREFERENCEDIALOG_H

#ifndef CSVPARSER_H
#define CSVPARSER_H

#include <QObject>
#include <QStandardItemModel>

class CsvParser : public QObject
{
    Q_OBJECT
public:
    explicit CsvParser(QObject *parent = 0);
    explicit CsvParser(const QString &filename);
    QStandardItemModel *parseFromFile(const QString &filename, const QString &codec = "Shift-JIS");
    void writeCSV(const QStandardItemModel *model, QString filename_in = "");
signals:

public slots:

private:
    void parse(const QString &string);
    QString initString(const QString &string);
    bool parseHeader;

    QList<QStringList> csv;
    QStandardItemModel *model;
    QList<QStandardItem*> standardItemList;
};

#endif // CSVPARSER_H

#-------------------------------------------------
#
# Project created by QtCreator 2014-06-10T22:20:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OmniCSV
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    omniaxitemmodel.cpp \
    csvparser.cpp \
    csvinspector.cpp \
    preferencedialog.cpp \
    angle_setting/adddialog.cpp \
    angle_setting/anglecontrol.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    omniaxitemmodel.h \
    csvparser.h \
    csvinspector.h \
    preferencedialog.h \
    angle_setting/adddialog.h \
    angle_setting/anglecontrol.h \
    version.h \
    aboutdialog.h

FORMS    += \
    angle_setting/adddialog.ui \
    aboutdialog.ui

RESOURCES += \
    icons.qrc

ICON = $$_PRO_FILE_PWD_/head.icns


#SUBDIRS += \
#    angle_setting/angle_setting.pro
